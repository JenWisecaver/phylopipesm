#!/bin/sh
export CWD=$PWD

### USER SPECIFIED VARIABLES ###
QUERY="$CWD/example/Ehellem_query.fa"				# path to input gene or protein fasta file
DB='nr'   								# blast database to search against (specify path the DB using $BLASTDB, see README)

# SLURM variables
USER='jen.wisecaver@vanderbilt.edu'		# user email
NODES=1								
NTASKS=1
MEM=19200
CPU=8
THREADS=16
TIME='48:0:0'							

# BLAST parameters
PROGRAM="blastp"		# what flavor of blast do you want to run?
EVAL='1e-3'    			# evalue threshold 
MAXBLAST=1000				# blast output max seqs

# BLAST parsing parameters
MINSEQ=3		# minimum number of homologs to print to fasta file 
MAXSEQ=200		# maximum number of homologs to print to fasta file
MAXPERSP=2		# maximum number of homologs to print per NCBI taxonomy ID
LDIFF=5 		# maximum order of magnitude difference between query and subject sequence lengths
TAXDIR='/data/wisecajh/blastdbs'

# Alignment parameters
MINLEN=100																				# minimum length of alignment. If shorter after trimming, tree will not be built.
CDOPT=0.95
MAFFTOPT="--reorder --bl 30 --op 1.0 --maxiterate 1000 --retree 1 --genafpair --quiet"	# mafft options
RUN_TRIMAL='Y'																		
TRIMOPT="-gappyout"																		# trimal options

# Tree building parameters
TREETYPE='iqtree'																	# type of tree: 'fasttree', 'raxml', 'iqtree', or 'all'
FASTTREEOPT="-spr 4 -mlacc 2 -slownni" 												# fasttree options
RAXMLOPT='-f a -m PROTGAMMAAUTOF -x 12345 -p 12345 -N 100'							# raxml options
IQTREEOPT='-m TEST -alrt 1000 -bb 1000'
COLLAPSE="java -d64 -Xms1024M -Xmx1500M -jar /home/wisecajh/applications/TreeCollapseCL4.jar"
RXSUPPORT=80
FTSUPPORT=0.95
IQSUPPORT=95

# Environmental variables. You can specify the path to the executables if not in your $PATH
CDHIT='cd-hit'
MAFFT='mafft'				# path to mafft executable 
TRIMAL='trimal'				# path to trimal executable 
FASTTREE='FastTree'			# path to fasttree executable 
RAXML='raxmlHPC-SSE3'		# path to raxml executable
IQTREE='iqtree-omp'

### Directory information ###
SCRIPTS="$CWD/scripts"
BOUTDIR="$CWD/blast-out"     # output directory for blastfiles
SLURMDIR="$CWD/slurm-out"

echo "Creating working directories"
if [ ! -d "$BOUTDIR" ]; then mkdir $BOUTDIR; fi
if [ ! -d "$SLURMDIR" ]; then mkdir  $SLURMDIR; fi
if [ ! -d "results" ]; then mkdir  results; fi
if [ ! -d "results/fasta" ]; then mkdir  results/fasta; fi
if [ ! -d "results/cdhit" ]; then mkdir  results/cdhit; fi
if [ ! -d "results/tree" ]; then mkdir  results/tree; fi
if [ ! -d "results/mafft" ]; then mkdir  results/mafft; fi
if [ ! -d "results/reord" ]; then mkdir  results/reord; fi
if [ ! -d "results/trimal" ]; then mkdir  results/trimal; fi

export CWD PROGRAM CPU THREADS EVAL QUERY DB SCRIPTS BOUTDIR SLURMDIR
export MAXBLAST MINSEQ MAXSEQ MAXPERSP LDIFF TAXDIR
export MAXLEAVES MINLEN RUN_TRIMAL TREETYPE MAFFTOPT TRIMOPT CDOPT FASTTREEOPT RAXMLOPT COLLAPSE RXSUPPORT FTSUPPORT IQTREEOPT IQSUPPORT
export MAFFT TRIMAL FASTTREE RAXML CDHIT IQTREE

##  1 - Submit blast job
echo "Submitting blast job -N blast $SCRIPTS/run_blast.sh"
FIRST=`sbatch --account=rokaslab_burst --job-name=blast --mail-user=$USER --nodes=1 --ntasks=1 --cpus-per-task=$CPU --mem=$MEM --time=$TIME $SCRIPTS/run_blast.sh | sed 's/Submitted batch job //'`

##  2 - Parse blast outputs 
echo "Submitting treebuilding job -N tree $SCRIPTS/run_buildtrees.sh"
SECOND=`sbatch --account=rokaslab_burst --dependency=afterok:$FIRST --job-name=tree --mail-user=$USER --nodes=1 --ntasks=1 --cpus-per-task=$CPU --mem=$MEM --time=$TIME $SCRIPTS/run_buildtrees.sh | sed 's/Submitted batch job //'`

echo "All jobs submitted."


