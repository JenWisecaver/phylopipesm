# Build gene trees for a set of sequences based on BLAST inferred homology #

This pipeline is best for smaller jobs with 1-100 query sequences. For larger jobs use the job array based phylopipeLG (coming soon).

The **example/** directory contains E. hellem predicted proteins. Default values will blast **Ehellem_query.fa** against NCBI's refseq and parse the results to construct a phylogenetic tree for each query sequence. 

Query sequences from [Pombert et al. 2012 PNAS](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3412028/) showing different evolutionary histories of gene trees in E. hellem

### USAGE ###

Create a working copy of the phylopipeSM directory 

```
cp -r phylopipeSM/ [working_dir]
```

Create a blastable database of your query sequences. You must parse the sequence IDs and specify the NCBI taxonomy ID. If running the reciprocal best blast search (RUN_RBBH=Y), the database should be constructed using the query's entire proteome, otherwise you can use just the sequences you are building trees for. This step is already done for the E. hellem example.

```
cd phylopipeSM/example/
makeblastdb -in Ehellem_pep -dbtype prot -parse_seqids -taxid 907965
```

Modify the following lines in the PhyloPipeline.sh file as necessary
```
#!bash
QUERY="$CWD/example/Ehellem_query.fa"				# path to input gene or protein fasta file
QUERYDB="$CWD/example/Ehellem_pep"  				# path to input sequences as a blastable database (see README)
DB='refseq_protein'   								# blast database to search against (specify path the DB using $BLASTDB, see README)

# PBS variables
USER='jen.wisecaver@vanderbilt.edu'		# user email
NCPUS='nodes=1:ppn=1'					# resource request for low memory/single processor jobs
MEM='mem=2000mb'						# memory request for low memory/single processor jobs
CPUT='cput=120:0:0'						# CPU time request for low memory/single processor jobs 			
HNCPUS='nodes=1:ppn=8'					# resource request for high memory/multi processor jobs
HMEM='mem=60000mb'						# memory request for high memory/multi processor jobs
HCPUT='cput=960:0:0'					# CPU time request for high memory/multi processor jobs
WT='walltime=120:0:0'					# wall time

# BLAST parameters
PROGRAM="blastp"		# what flavor of blast do you want to run?
EVAL='1e-3'    			# evalue threshold 
CPU=8					# number of CPUs to use per job, should match whatever is specified in $HNCPUS 
MAXSEQ=1000				# blast output max seqs

# BLAST parsing parameters
MINSEQ=4		# minimum number of homologs to print to fasta file 
MAXSEQ=200		# maximum number of homologs to print to fasta file
MAXPERSP=1		# maximum number of homologs to print per NCBI taxonomy ID
LDIFF=5 		# maximum order of magnitude difference between query and subject sequence lengths
RUN_RBBH='Y'	# run reciprocal best blast analysis 'Y' or 'N'
MINBIT=0.6		# minimum allowable bitscore ratio in rbbh analysis 
MAXSKIP=10		# maximum number of skipped paralogs 

# Tree building parameters
MAXLEAVES=300																	# maximum number of homologs to retain based on guide tree topology. Set MAXLEAVES greater than MAXSEQ to not crop guide tree.
MINLEN=100																		# minimum length of alignment. If shorter after trimming, tree will not be built.
TREETYPE='fasttree'																# type of tree either 'fasttree' or 'raxml'
MAFFTOPT="--reorder --bl 30 --op 1.0 --maxiterate 1000 --retree 1 --genafpair --thread -1"	# mafft options
TRIMOPT="-automated1"															# trimal options
FASTTREEOPT="-wag -spr 4 -mlacc 2 -slownni" 									# fasttree options
RAXMLOPT='-f a -m PROTGAMMAAUTOF -x 12345 -p 12345 -N 100'						# raxml options
```

Execute PhyloPipeline.sh

```
./PhyloPipeline.sh
```

### OUTPUT ###

The **blast-out/** directory contains the blast output.

The **results/** directory contains the intermediate alignment files and the final tree file

1. **fasta/** FASTA files to align containing full sequences parsed form blast output
2. **reord/** Sequences are reordered based on global sequence similarity
3. **crop/** Reduced set of sequences based on guide tree
4. **mafft/** Aligned sequences
5. **trimal/** Trimmed alignments
6. **tree/** Phylogenetic trees

Lastly, the **pbsout/** directory contains the PBS output and error files.