#!/usr/local/bin/perl
use warnings;
use strict;
use Getopt::Std;
use Cwd;
use Bio::SeqIO;

###############################################
#
# post_cdhit_cleanup.pl
# 
# Jen Wisecaver
# 20160807 
#
# Run: perl extract_sequences.pl  [OPTIONS]
#
################################################

# usage
my $dir = getcwd;
my %opts; getopt('fg', \%opts );

my $usage = "\n\tUsage Error:
	Run: perl post_cdhit_cleanup.pl -f [fastafile] -g [query]

	OPTIONS =
	Files:
	-f: <FULL_FILENAME> fastafile: path to fastfile for treebuilding (see README)
	-g: <FULL_FILENAME> QUERY: path to fasta query (see README)
	
	\n\n";

# Files
my $FASTA;
if ($opts{f}) { 
	$FASTA = "$opts{f}";
}
else { die "\n\tInput <f>$usage"; }

my $QUERY;
if ($opts{g}) { 
	$QUERY = "$opts{g}";
}
else { die "\n\tInput <g>$usage"; }


$FASTA =~ /^(.*)\.fa$/;
my $qseqid = $1;
my $outfile = $qseqid . '.clean';
open (OUT, '>', $outfile) or die "Couldn't open file $outfile: $!\n";

#get query sequence from query genome
my $qseq = '';
my $inseqIO = Bio::SeqIO->new(-file => "$QUERY", -format => 'fasta');
while ( my $seqIO = $inseqIO->next_seq() ) {
	my $name = $seqIO->display_id();
	if ($name eq $qseqid){
		my $sequence = $seqIO->seq();
		$qseq = $sequence;
		last;
	}
}

print OUT ">QUERY_${qseqid}\n$qseq\n";

my $fasseqIO = Bio::SeqIO->new(-file => "$FASTA", -format => 'fasta');
while ( my $seqIO = $fasseqIO->next_seq() ) {
	my $name = $seqIO->display_id();
	next if ($name =~ /^QUERY/);
	my $sequence = $seqIO->seq();

	print OUT ">$name\n$sequence\n";	
}

close OUT;