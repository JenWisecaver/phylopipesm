#!/bin/bash
### Request email when job begins and ends
#SBATCH --mail-type=ALL
#SBATCH --output="slurm-out/tree-%j.out"

echo -e "SLURM job ID: $SLURM_JOBID\n"
PARALLEL="/home/wisecajh/bin/parallel --delay 1 --jobs $THREADS"

##############################
### Parse the blast output ###
##############################
PROGRAM="perl $CWD/scripts/extract_sequences.pl"
PARSE="perl $CWD/scripts/parse_blastfile.pl"
BLASTFIL="$CWD/blast-out/blastout.txt"
IN="$CWD/results/fasta"

cd $IN

echo -e "\nParsing blastfile $BLASTFIL ...........................................................................................\n"
echo -e "COMMAND: $PARSE $BLASTFIL\n"
$PARSE $BLASTFIL

echo -e "Extracting full length sequences from $DB ...........................................................................................\n"
echo -e "PARAMETERS: -d $DB -g $QUERY -n $MINSEQ -x $MAXSEQ -a $MAXPERSP -l $LDIFF -t $TAXDIR\n" 
export PROGRAM DB QUERY MINSEQ MAXSEQ MAXPERSP LDIFF TAXDIR
extract() {
	echo -e "COMMAND: $PROGRAM -b $1\n"
	$PROGRAM -b $1 -d $DB -g $QUERY -n $MINSEQ -x $MAXSEQ -a $MAXPERSP -l $LDIFF -t $TAXDIR
}
export -f extract
ls *txt | $PARALLEL extract


##############################################
### Collapse similar sequences with cd-hit ###
##############################################
IN="$CWD/results/fasta"
OUT="$CWD/results/cdhit"

cd $IN

echo -e "\nCollapsing similar sequences (threshold = $CDOPT) in $IN using $CDHIT...........................................................................................\n"
ls *fa | $PARALLEL $CDHIT -i {} -o $OUT/{} -c $CDOPT

# make sure query sequence is the first sequence
cd $OUT
ls *fa | /home/wisecajh/bin/parallel perl $CWD/scripts/post_cdhit_cleanup.pl -f {} -g $QUERY
ls *clean | /home/wisecajh/bin/parallel mv {} {.}.fa

################################################################
### Reorder the extracted sequences based on global homology ###
################################################################
ROUGHOPTS="--retree 0 --treeout --reorder --6merpair --averagelinkage --quiet"
ACCURATEOPTS="--retree 0 --treeout --reorder --globalpair --weighti 0 --gop -1.0 --gep 0.1 --gexp -0.1 --averagelinkage --quiet"
ACCURATEMAX=500

IN="$CWD/results/cdhit"
OUT="$CWD/results/reord"

cd $IN
echo -e "\nReordering extracted sequences in $IN ...........................................................................................\n"

export IN OUT MAFFT ROUGHOPTS ACCURATEOPTS 
reorder() {
	COUNT=`grep ">" $1 | wc -l`
	NL=$'\n'
	COUNT=${COUNT%$NL}
	
	echo -e "FLAG: $COUNT sequences in $1"	
	if [[ "$COUNT" -gt "$ACCURATEMAX" ]]; then
		echo -e "COMMAND: $MAFFT $ROUGHOPTS $IN/$1 $OUT/$1\n"
		$MAFFT $ROUGHOPTS $IN/$1 > $OUT/$1
	else 
		echo -e "COMMAND: $MAFFT $ACCURATEOPTS $IN/$1 $OUT/$1\n"
		$MAFFT $ACCURATEOPTS $IN/$1 > $OUT/$1
	fi
}
export -f reorder
ls *fa | $PARALLEL reorder 

###########################
### Align the sequences ###
###########################
IN="$CWD/results/cdhit"
OUT="$CWD/results/mafft"

cd $IN

echo -e "Aligning sequences in $IN ...........................................................................................\n"
export MAFFT MINSEQ MAFFTOPT IN OUT
alignseqs () {
	COUNT=`grep ">" $1 | wc -l`
	NL=$'\n'
	COUNT=${COUNT%$NL}
	
	echo -e "FLAG: $COUNT sequences in $1"	

	if [[ "$COUNT" -ge "$MINSEQ" ]]; then
		echo -e "COMMAND: $MAFFT $MAFFTOPT $IN/$1 $OUT/$1\n"
		$MAFFT $MAFFTOPT $IN/$1 > $OUT/$1
	fi
} 
export -f alignseqs
ls *fa | $PARALLEL alignseqs


##########################
### Trim the sequences ###
##########################
IN="$CWD/results/mafft"
OUT="$CWD/results/trimal"

cd $IN

if [[ "$RUN_TRIMAL" == 'y'  ]] || [[ "$RUN_TRIMAL" == 'Y'  ]]; then
	echo -e "\nTrimming sequences in $IN using $TRIMAL ...........................................................................................\n"
	ls *fa | $PARALLEL $TRIMAL -in {} -out $OUT/{} $TRIMOPT
fi


####################################
### Build the phylogenetic trees ###
####################################
PRE="$CWD/results/mafft"
if [[ "$RUN_TRIMAL" == 'y'  ]] || [[ "$RUN_TRIMAL" == 'Y'  ]]; then
	IN="$CWD/results/trimal"
else
	IN="$CWD/results/mafft"
fi
OUT="$CWD/results/tree"

cd $IN

echo -e "\nBuilding trees for files in $IN ...........................................................................................\n"
export PRE IN OUT CWD TREETYPE FASTTREE FASTTREEOPT RAXML RAXMLOPT IQTREE IQTREEOPT
buildtrees () {
	PRENUM=`grep ">" $PRE/$1 | wc -l`
	NEWNUM=`grep ">" $IN/$1 | wc -l`
	PRELEN=`perl $CWD/scripts/max_seq_len.pl $PRE/$1`
	NEWLEN=`perl $CWD/scripts/max_seq_len.pl $IN/$1`
	echo -e "FLAG: $1 alignment length pre trimming = $PRELEN"
	echo -e "FLAG: $1 alignment length post trimming = $NEWLEN"
	echo -e "FLAG: $1 seq count pre trimming = $PRENUM"
	echo -e "FLAG: $1 seq count post trimming = $NEWNUM\n"
	
	if [[ "$NEWLEN" -ge "$MINLEN" ]]; then
	
		if [[ "$TREETYPE" == 'fasttree' ]]; then
			echo -e "COMMAND: $FASTTREE $FASTTREEOPT -log $OUT/${1}.log $IN/$1 $OUT/${1}.tre\n"
			$FASTTREE $FASTTREEOPT -log $OUT/${1}.log $IN/$1 > $OUT/${1}.tre
		fi

		if [[ "$TREETYPE" == 'raxml' ]]; then
			echo -e "COMMAND: $RAXML -s $1 -n ${1}.out $RAXMLOPT\n"
			$RAXML -s $1 -n ${1}.out $RAXMLOPT
			mv *.${1}.out $OUT
		fi

		if [[ "$TREETYPE" == 'iqtree' ]]; then
			echo -e "COMMAND: $IQTREE -s $IN/$1 $IQTREEOPT -pre $OUT/${1} -nt 1\n"
			$IQTREE -s $IN/$1 $IQTREEOPT -pre $OUT/${1} -nt 1
		fi

		if [[ "$TREETYPE" == 'all3' ]]; then
			echo -e "COMMAND: $FASTTREE $FASTTREEOPT -log $OUT/${1}.log $IN/$1 $OUT/${1}.tre\n"
			echo -e "COMMAND: $IQTREE -s $IN/$1 $IQTREEOPT -pre $OUT/${1} -nt 1\n"
			echo -e "COMMAND: $RAXML -s $1 -n ${1}.out $RAXMLOPT\n"
			$FASTTREE $FASTTREEOPT -log $OUT/${1}.log $IN/$1 > $OUT/${1}.tre
			$IQTREE -s $IN/$1 $IQTREEOPT -pre $OUT/${1} -nt 1
			$RAXML -s $1 -n ${1}.out $RAXMLOPT
			mv *.${1}.out $OUT
		fi
	fi
}
export -f buildtrees
ls *fa | $PARALLEL buildtrees

##################################################################
### Midpoint root trees and collapse branches with low support ###
##################################################################
cd $OUT

REROOT="python $CWD/scripts/root_tree.py"

ls RAxML_bipartitions.*out | $PARALLEL mv {} {}.tre
ls *.fa.contree | $PARALLEL mv {} {}.tre

echo -e "\nCollapsing branches with low support in trees in $OUT ...........................................................................................\n"
export COLLAPSE RXSUPPORT FTSUPPORT IQSUPPORT
modtrees () {
	if [[ "$1" =~ 'RAxML' ]]; then
		echo -e "COMMAND: $COLLAPSE -b $RXSUPPORT -f $1\n"
		$COLLAPSE -b $RXSUPPORT -f $1
	elif [[ "$1" =~ 'contree' ]]; then
		echo -e "COMMAND: $COLLAPSE -b $IQSUPPORT -f $1\n"
		$COLLAPSE -b $IQSUPPORT -f $1
	else
		echo -e "COMMAND: $COLLAPSE -b $FTSUPPORT -f $1\n"
		$COLLAPSE -b $FTSUPPORT -f $1
	fi
}
export -f modtrees
ls *.tre | $PARALLEL modtrees

echo -e "\nRerooting trees in $OUT ...........................................................................................\n"
ls *coll.fa.*tre | $PARALLEL $REROOT
