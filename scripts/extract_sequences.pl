#!/usr/local/bin/perl
use warnings;
use strict;
use Getopt::Std;
use Cwd;
use Bio::SeqIO;

###############################################
#
# extract_sequences.pl 
# 
# Jen Wisecaver
# 20141107 
#
# Run: perl extract_sequences.pl  [OPTIONS]
#
################################################

# usage
my $dir = getcwd;
my %opts; getopt('bdgnxalhckt', \%opts );

my $usage = "\n\tUsage Error:
	Run: perl extract_sequences.pl -b [blastfile] -d [database] -q [query] [options]

	OPTIONS =
	Files:
	-b: <FULL_FILENAME> BLASTFILE: path to BLAST output in custom tab delimited format (see README)
	-d: <FILENAME> DB: BLAST database
	-g: <FULL_FILENAME> QUERY: path to fasta query for RBH check in blastdb format (see README)
	
	General:
	-n: <INTEGER> MINSEQ: minimum number of homologs to print to fasta file (default = 4)
	-x: <INTEGER> MAXSEQ: maximum number of homologs to print to fasta file (default = 100)
	-a: <INTEGER> MAXPERSP: maximum number of homologs to print per NCBI taxonomy ID (default = 1)
	-l: <INTEGER> LDIFF: Maximum order of magnitude difference between query and subject sequence lengths (default = 5)
	-t: <DIRNAME> TAXDIR: path to directory containing the ncbi taxonomy files 'nodes.dmp' and 'merged.dmp'

	\n\n";

# Files
my $BLASTFILE;
if ($opts{b}) { 
	$BLASTFILE = "$opts{b}";
}
else { die "\n\tInput <b>$usage"; }

my $DB;
if ($opts{d}) { 
	$DB = "$opts{d}";
}
else { die "\n\tInput <d>$usage"; }

my $QUERY;
if ($opts{g}) { 
	$QUERY = "$opts{g}";
}
else { die "\n\tInput <g>$usage"; }

# General
my $MINSEQ; 
if ($opts{n}) { if ($opts{n} =~ m/^\d+$/) { 
	if (int($opts{n}) eq $opts{n}) { $MINSEQ = "$opts{n}"; }} else { die "\n\tInput <n>$usage"; } }
else { $MINSEQ = 4; }

my $MAXSEQ; 
if ($opts{x}) { if ($opts{x} =~ m/^\d+$/) { 
	if (int($opts{x}) eq $opts{x}) { $MAXSEQ = "$opts{x}"; }} else { die "\n\tInput <x>$usage"; } }
else { $MAXSEQ = 100; }

my $MAXPERSP; 
if ($opts{a}) { if ($opts{a} =~ m/^\d+$/) { 
	if (int($opts{a}) eq $opts{a}) { $MAXPERSP = "$opts{a}"; }} else { die "\n\tInput <a>$usage"; } }
else { $MAXPERSP = 1; }

my $LDIFF; 
if ($opts{l}) { if ($opts{l} =~ m/^\d+$/) { 
	if (int($opts{l}) eq $opts{l}) { $LDIFF = "$opts{l}"; }} else { die "\n\tInput <l>$usage"; } }
else { $LDIFF = 5; }

my $TAXDIR;
if ($opts{t}) { 
	$TAXDIR = "$opts{t}";
}
my $nodes_file = "$TAXDIR/nodes.dmp";
my $merged_file = "$TAXDIR/merged.dmp";
open (NFIL, '<', $nodes_file) or die "Couldn't open file $nodes_file: $!\n";
open (MFIL, '<', $merged_file) or die "Couldn't open file $merged_file: $!\n";


# outfmt: qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore saccn slen qlen nident positive staxids qcovs"
open (IFIL, '<', $BLASTFILE) or die "Couldn't open file $BLASTFILE: $!\n";

#######################################################
#### initialize global taxonomy and lineage hashes ####
#######################################################
my %taxnodes;
my %lin;
poplin();

while ( my $line = <NFIL> ) {
	chomp $line;
	$line =~ /^(\d+)\t\|\t(\d+)\t\|/;
	$taxnodes{$1} = $2;
}
close NFIL;

while ( my $line = <MFIL> ) {
	chomp $line;
	$line =~ /^(\d+)\t\|\t(\d+)\t\|/;
	$taxnodes{$1} = $2;
}
close MFIL;

my %fin_hash;
my %seq_hash;
my $skip_counter = 0;
my $seq_counter = 0;
my %sname_counter;


(my $qseqid = $BLASTFILE) =~ s/\.txt$//;

my $logfile = $qseqid . '.log';
open (LOG, '>', $logfile) or die "Couldn't open file $logfile: $!\n";

# Parse BLAST
print "parsing blasthits to $qseqid .......................................\n";
print LOG "parsing blasthits to $qseqid .......................................\n";
while ( my $line = <IFIL> ) {
	chomp $line;
	my @col = split(/\t/, $line);
	my $sseqid = $col[1];

	my $saccn;
	if ($sseqid =~ /^[^\|]+\|[^\|]+\|[^\|]+\|([^\|]+)/){
		$saccn = $1;
	}else{
		$saccn = $sseqid;
	}

	# Exit loop if already reached maximum number of saved sequences
	last if ($seq_counter >= $MAXSEQ);
	
	#get subject description
	my $sdesc = `blastdbcmd -db $DB -entry $saccn -outfmt "%t" | head -n 1`;
	chomp $sdesc;
	next if ($sdesc =~ /MULTISPECIES/);
			
	#get subject sequence from db
	my $sseq = `blastdbcmd -db $DB -entry $saccn -outfmt "%s" | head -n 1`;
	chomp $sseq;
	$sseq =~ s/\W//g;	

	#get subject taxonomy id and scientific name
	my @staxids = split(/;/, $col[17]);
	my $staxid = $staxids[-1];
	my @snames = split(/;/, $col[18]);
	my $sname = $snames[-1];
	$sname =~ s/\W/_/g;	
	$sname =~ s/__/_/g;
	my $slin_name = 'Not_specified';

	#get subject taxonomy tree	
	my $tax_tree = taxdump($staxid);
	my @ncbi_lineage = split(/,/, $tax_tree);
	my %ncbi_lineage_hash;

	#get subject group name and store lineage in hash
	foreach my $taxon_id (@ncbi_lineage) {
		if (exists $lin{$taxon_id} && $slin_name eq 'Not_specified'){
			$slin_name = $lin{$taxon_id};
			last;
		}
	}

	unless ($staxid =~ /^\d+$/){
		print LOG "WARNING: $qseqid\t$sseqid\t$staxid\ttaxonomy ID undefined skipping...\n";
		next;		
	}
	
	if ($slin_name eq 'Not_specified'){
		print LOG "SKIP: Unspecified taxonomic lineage for $sname $saccn...\n";
		next;
	}
	
	#skip subject if already reached maximum number of saved sequences for that species
	if ( exists $sname_counter{$sname} ){ 
		if ($sname_counter{$sname} >= $MAXPERSP){
			print LOG "SKIP: Maximum homologs ($MAXPERSP) already printed for $sname, skipping Subject: $slin_name $sname $saccn...\n";
			next;
		}
	}

	#filter based on subject length relative to query length
	my $qlen = $col[14];
	my $slen = $col[13];
	my $qdiv = $slen/$qlen;
	my $sdiv = $qlen/$slen;
	if ( $qdiv > $LDIFF || $sdiv > $LDIFF ){
		print LOG "SKIP: Subject length $slen and query ($qseqid) length $qlen are too different, skipping Subject: $slin_name $sname $saccn...\n";
		next;
	}
		
	#don't retain identical sequences, but print GIs to log file. 
	if (exists $seq_hash{$sname}{$sseq}){
		print LOG "SKIP: Identical seqs for query $qseqid, stored subject:$seq_hash{$sname}{$sseq}, skipping subject: $slin_name $sname $saccn...\n";
		next;
	}
	$seq_hash{$sname}{$sseq} = $saccn;
	
	@{$fin_hash{$saccn}} = ( $sname , $slin_name , $sseq , $sdesc, $staxid );
	$seq_counter++;
	$sname_counter{$sname}++;
	
}
close IFIL;

print "printing homologs to fasta file .............................................\n";
print LOG "\nprinting homologs to fasta file ............................................\n";

if ($seq_counter < $MINSEQ){
	print LOG "WARNING: Too few seqs, skipping $qseqid\n";
	die;
}

my $outfile = $qseqid . '.fa';
open (OUT, '>', $outfile) or die "Couldn't open file $outfile: $!\n";

#get query sequence from query genome
my $qseq = '';
my $inseqIO = Bio::SeqIO->new(-file => "$QUERY", -format => 'fasta');
while ( my $seqIO = $inseqIO->next_seq() ) {
	my $name = $seqIO->display_id();
	if ($name eq $qseqid){
		my $sequence = $seqIO->seq();
		$qseq = $sequence;
		last;
	}
}

print OUT ">QUERY_${qseqid}\n$qseq\n";
print LOG "Writing to fastafile: $outfile\n\n";

#print blast hit full length sequences
foreach my $saccn (keys %fin_hash){
	
	if ($qseqid eq $saccn){next;}
	
	my $sname = $fin_hash{$saccn}[0];	
	$sname = substr($sname, 0, 20);
	$sname =~ s/_+$//;
	my $slin_name = $fin_hash{$saccn}[1];	
	my $sseq = $fin_hash{$saccn}[2];	
	my $sdesc = $fin_hash{$saccn}[3];
	my $staxid = $fin_hash{$saccn}[4];

	next if ($qseq eq $sseq); #to do: add a query taxonomic id flag and only skip if the query and subject have the same taxid
	
	print OUT ">$saccn" . '-' . "$staxid" . '-' . "$sname" . '-' . "$slin_name\n$sseq\n";
	print LOG "Hit\t$saccn\t$sdesc\n";
			
}

close LOG;
close OUT;

sub taxdump {
	my $taxid = shift;
	my $root;
	my @taxlinarr;
	while (!defined $root){
		if (!defined $taxid){
			last;
		}
		if ($taxid == 1){$root = 1};
		push @taxlinarr, $taxid;
		$taxid = $taxnodes{$taxid};
	}
	my $taxlin = join(",", @taxlinarr);
	return $taxlin;
}

sub poplin {

	%lin = (
		"2759", "other_Eukaryota",
		"33634", "other_Stramenopiles",
		"2836", "Bacillariophyta",
		"4762", "Oomycetes",
		"33630", "other_Alveolata",
		"5794", "Apicomplexa",
		"5878", "Ciliophora",
		"2864", "Dinophyceae",
		"554915", "Amoebozoa",
		"554296", "Apusozoa",
		"1401294", "Breviatea",
		"193537", "Centroheliozoa",
		"3027", "Cryptophyta",
		"33682", "Euglenozoa",
		"207245", "Fornicata",
		"38254", "Glaucocystophyceae",
		"2830", "Haptophyceae",
		"5752", "Heterolobosea",
		"556282", "Jakobida",
		"339960", "Katablepharidophyta",
		"136087", "Malawimonadidae",
		"33154", "other_Opisthokonta",
		"28009", "Choanoflagellida",
		"4751", "other_Fungi",
		"6029", "Microsporidia",
		"147537", "Saccharomycotina",
		"147538", "other_Pezizomycotina",
		"147541", "Dothideomycetes",
		"147545", "Eurotiomycetes",
		"147550", "Sordariomycetes",
		"147548", "Leotiomycetes",
		"451866", "Taphrinomycotina",
		"5204", "Basidiomycota",
		"451864", "other_Dikarya",
		"4890", "other_Ascomycota",
		"33208", "Metazoa",
		"66288", "Oxymonadida",
		"5719", "Parabasalia",
		"543769", "other_Rhizaria",
		"136419", "Cercozoa",
		"29178", "Foraminifera",
		"2763", "Rhodophyta",
		"33090", "other_Viridiplantae",
		"3041", "Chlorophyta",
		"35493", "Streptophyta",
		"2157", "other_Archaea",
		"743724", "Aenigmarchaeota",
		"28889", "Crenarchaeota",
		"743725", "Diapherotrites",
		"28890", "Euryarchaeota",
		"1448933", "Geoarchaeota",
		"51967", "Korarchaeota",
		"192989", "Nanoarchaeota",
		"1462430", "Nanohaloarchaeota",
		"1462422", "Parvarchaeota",
		"651137", "Thaumarchaeota",
		"2", "other_Bacteria",
		"201174", "Actinobacteria",
		"200783", "Aquificae",
		"67819", "Armatimonadetes",
		"68336", "BacteroidetesChlorobi",
		"67814", "Caldiserica",
		"51290", "ChlamydiaeVerrucomicrobia",
		"200795", "Chloroflexi",
		"200938", "Chrysiogenetes",
		"1117", "Cyanobacteria",
		"200930", "Deferribacteres",
		"1297", "DeinococcusThermus",
		"68297", "Dictyoglomi",
		"74152", "Elusimicrobia",
		"131550", "FibrobacteresAcidobacteria",
		"1239", "Firmicutes",
		"32066", "Fusobacteria",
		"142182", "Gemmatimonadetes",
		"1293497", "Nitrospinae",
		"40117", "Nitrospirae",
		"203682", "Planctomycetes",
		"1224", "Proteobacteria",
		"203691", "Spirochaetes",
		"508458", "Synergistetes",
		"544448", "Tenericutes",
		"200940", "Thermodesulfobacteria",
		"200918", "Thermotogae",
		"12884", "Viroids",
		"10239", "Viruses",
	);

}