#!/usr/local/bin/perl
use Bio::Perl;
use strict;
use warnings;
use Bio::SeqIO;

my $infile = $ARGV[0];

my $inseqIO = Bio::SeqIO->new(-file => "$infile", -format => 'fasta');

my $max = 0;
my $seqIO = $inseqIO->next_seq();
my $sequence = $seqIO->seq();
	
my $length = length($sequence);
	
print "$length";