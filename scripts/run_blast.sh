#!/bin/sh
### Request email when job begins and ends
#SBATCH --mail-type=ALL
#SBATCH --output="slurm-out/blast-%j.out"

echo Path to BLAST databases: $BLASTDB
echo SLURM job ID: $SLURM_JOBID

# for blast
BOUTPUT="$BOUTDIR/blastout.txt"

cd $CWD

echo submitting: $PROGRAM -query $QUERY -out $BOUTPUT -db $DB -evalue $EVAL -num_threads $THREADS -max_target_seqs $MAXBLAST -outfmt '6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore saccver slen qlen nident positive staxids sscinames'

time $PROGRAM -query $QUERY -out $BOUTPUT -db $DB -evalue $EVAL -num_threads $THREADS -max_target_seqs $MAXBLAST -outfmt '6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore saccver slen qlen nident positive staxids sscinames'

