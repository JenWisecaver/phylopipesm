#!/usr/local/bin/perl
use strict;
use warnings;
use Bio::SeqIO;
use Bio::TreeIO;

my $trefile = $ARGV[0];
my $fastafile = $ARGV[1];
my $max_leaves = $ARGV[2];

my $target_leaf = `head -1 $fastafile`;
chomp $target_leaf;
$target_leaf =~ s/^>/1_/;

# .............. open the fasta file and store in hash
my %seqs;
my $inseqIO = Bio::SeqIO->new(-file => "$fastafile", -format => 'fasta');
while ( my $seqIO = $inseqIO->next_seq() ) {
	
	my $name = $seqIO->display_id();
	my $sequence = $seqIO->seq();
	
	$seqs{$name} = $sequence;

}


# ............ Open treefile ...........
my $in = new Bio::TreeIO(-file => "$trefile", -format => 'newick');

my $tree = $in->next_tree;
my $query_leaf = $tree->find_node(-id => "$target_leaf");

my @lineage = $tree->get_lineage_nodes($query_leaf);
my @reverselin = reverse(@lineage);

my $target_node;
my $node_id = 0;
foreach my $node (@reverselin){
	$node_id++;
	my $leaf_count = 0;
	for my $child ( $node->get_all_Descendents ) {
		if ($child->is_Leaf){
			$leaf_count++;
		}		  	
	}
	if ( $leaf_count <= $max_leaves ){
		$target_node = $node;
	}else{
		last;
	}
}

if (defined $target_node){
	for my $child ( $target_node->get_all_Descendents ) {
		if ($child->is_Leaf){
			my $name = $child->id;
			$name =~ s/^\d+_//;
			my $sequence = $seqs{$name};
			print ">$name\n$sequence\n";
		}		  	
	}
}




