#!/usr/local/bin/perl
use warnings;
use strict;

my $blastfile = $ARGV[0];

# blast outfmt: qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore saccn slen qlen nident positive staxids qcovs"
open (IFIL, '<', $blastfile) or die "Couldn't open file $blastfile: $!\n";

my %hash;
my %bitscores;
# Parse BLAST
while ( my $line = <IFIL> ) {
	my @col = split(/\t/, $line);
	
	my $qseqid = $col[0];
	$qseqid =~ s/^lcl\|//; 
	if ($qseqid =~ /^[^\|]+\|([^\|]+)/){
		$qseqid = $1;
	}

	
	my $sseqid = $col[1];
	my $bitscore = $col[11];

	unless (exists $hash{$qseqid}{$sseqid}){
		$hash{$qseqid}{$sseqid} = $line;
		$bitscores{$qseqid}{$sseqid} = $bitscore;
	}
}
close IFIL;


foreach my $qseqid (keys %hash){
	my $outfile = "$qseqid.txt";
	open (OFIL, '>', $outfile) or die "Couldn't open file $outfile: $!\n";
	foreach my $sseqid (sort { $bitscores{$qseqid}{$b} <=> $bitscores{$qseqid}{$a} } keys %{$bitscores{$qseqid}}){
		my $line = $hash{$qseqid}{$sseqid};
		print OFIL $line;
	}
	close OFIL;
}
