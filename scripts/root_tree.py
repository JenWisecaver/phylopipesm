#! /usr/bin/env python
#usage: script.py [tree to root in newick format]
import dendropy
import sys

tree = dendropy.Tree.get(path=sys.argv[1], schema='newick')
tree.is_rooted = False
tree.update_bipartitions()
# print("Before:")
# print(tree.as_string(schema='newick'))
# print(tree.as_ascii_plot(plot_metric='length'))

tree.reroot_at_midpoint(update_bipartitions=False)
tree.ladderize(ascending=False)

# print("After:")
# print(tree.as_string(schema='newick'))
# print(tree.as_ascii_plot(plot_metric='length'))

tree.write(path=sys.argv[1].replace('.tre', '.midpoint.tre'), schema="newick")


# from Bio import Phylo
# import sys
# def main(argv):
# 
# 	tree = Phylo.read(sys.argv[1], 'newick')
# 	#Phylo.draw_ascii(tree)
# 	#print tree
# 	tree.root_at_midpoint()
# 	
# 	Phylo.write(tree, sys.argv[1].replace('.tre', '.midpoint.tre'), 'phyxml') #the second argument is where you can modify the filename
# 	#Phylo.draw_ascii(tree)
# 	#print tree
# if __name__ == "__main__":
# 	main(sys.argv)
